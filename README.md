# ![Countdown logo](/android/app/src/main/res/mipmap-mdpi/ic_launcher_round.png) Countdown

A simple Flutter app for managing date countdowns.

<a href='https://play.google.com/store/apps/details?id=com.kylesferrazza.countdown&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img width="200px" alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>

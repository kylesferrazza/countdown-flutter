import 'package:countdown/countdown.dart';
import 'package:countdown/main.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';

class CreateCountdown extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Countdown"),
      ),
      body: CountdownForm(),
    );
  }
}

class CountdownForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => CountdownFormState();
}

class CountdownFormState extends State<CountdownForm> {
  TextEditingController nameController = TextEditingController();
  DateTime date;

  void complete(BuildContext context) {
    if (nameController.text.isEmpty) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Please enter a title for the countdown."),
      ));
      return;
    }
    if (date == null) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Please select a date and time for the countdown."),
      ));
      return;
    }
    ScopedModel.of<CountdownModel>(context)
        .add(Countdown.newID(this.nameController.text, date));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        children: <Widget>[
          TextField(
            controller: nameController,
            decoration: const InputDecoration(
              hintText: 'What should the countdown be called?',
              labelText: 'Countdown name',
            ),
          ),
          DateTimePickerFormField(
            inputType: InputType.both,
            format: DateFormat("EEEE, MMMM d, yyyy 'at' h:mma"),
            editable: true,
            decoration: InputDecoration(
                labelText: 'Date/Time', hasFloatingPlaceholder: false),
            onChanged: (dt) => setState(() => date = dt),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RaisedButton(
              onPressed: () {
                complete(context);
              },
              child: Text('Create'),
            ),
          ),
        ],
      ),
    );
  }
}

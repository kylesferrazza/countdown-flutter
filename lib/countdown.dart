import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:countdown/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:quiver/async.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:uuid/uuid.dart';

class Countdown extends StatefulWidget {
  @override
  Key get key => Key(this.uuid);

  final String name;
  final DateTime finish;
  final String uuid;

  Countdown.withID(this.name, this.finish, this.uuid);

  Countdown.newID(String name, DateTime finish)
      : this.withID(name, finish, Uuid().v1());

  @override
  State<StatefulWidget> createState() {
    return CountdownState(name, finish, uuid);
  }
}

class CountdownState extends State<Countdown> {
  final String name;
  final DateTime finish;
  final String uuid;
  StreamSubscription subscription;

  int days, hours, minutes, seconds;
  bool negative = false;

  void update() {
    Duration diff = this.finish.difference(DateTime.now());
    int newDays = diff.inDays.abs() % 365;
    int newHours = diff.inHours.abs() % 24;
    int newMinutes = diff.inMinutes.abs() % 60;
    int newSeconds = diff.inSeconds.abs() % 60;
    this.negative = diff.isNegative;
    this.days = newDays;
    this.hours = newHours;
    this.minutes = newMinutes;
    this.seconds = newSeconds;
  }

  CountdownState(this.name, this.finish, this.uuid) {
    this.update();
    this.subscription = Metronome.epoch(new Duration(seconds: 1)).listen((_) {
      setState(() {
        this.update();
      });
    });
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  void removeSelf(CountdownModel model) {
    model.remove(widget);
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text("'$name' removed."),
        action: SnackBarAction(
          label: "UNDO",
          onPressed: () => model.add(widget),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<CountdownModel>(
      rebuildOnChange: true,
      builder: (context, child, model) {
        return Dismissible(
          key: Key(uuid),
          direction: DismissDirection.startToEnd,
          onDismissed: (direction) {
            removeSelf(model);
          },
          background: Container(
            color: Colors.red,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 20.0),
            child: Icon(
              Icons.delete_sweep,
              color: Colors.white,
            ),
          ),
          child: Container(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Text(
                  name,
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    TimeBox(this.days, "days", negative),
                    TimeBox(this.hours, "hours", negative),
                    TimeBox(this.minutes, "mins", negative),
                    TimeBox(this.seconds, "secs", negative),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class TimeBox extends Widget {
  final int value;
  final String name;
  final bool negative;

  TimeBox(this.value, this.name, this.negative);

  @override
  Element createElement() {
    return Container(
      margin: const EdgeInsets.all(10.0),
      color: negative ? Colors.red : Colors.black,
      width: 55,
      height: 55,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            this.value.toString(),
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          Text(
            this.name + (negative ? " ago" : ""),
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ],
      ),
    ).createElement();
  }
}

import 'package:countdown/countdown.dart';
import 'package:countdown/create_countdown.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  var countdowns = CountdownModel();
  runApp(ScopedModel<CountdownModel>(
    model: countdowns,
    child: MyApp(),
  ));
}

class CountdownModel extends Model {
  var _countdowns = List<Countdown>();

  CountdownModel() {
    Future<SharedPreferences> futurePrefs = SharedPreferences.getInstance();
    futurePrefs.then((prefs) {
      Set<String> keys = prefs.getKeys();
      keys.forEach((uuid) {
        var lst = prefs.getStringList(uuid);
        String name = lst[0];
        int millis = int.parse(lst[1]);
        _countdowns
            .add(Countdown.withID(name, DateTime.fromMillisecondsSinceEpoch(millis), uuid));
        notifyListeners();
      });
    });
  }

  void add(Countdown countdown) {
    _countdowns.add(countdown);
    notifyListeners();
    Future<SharedPreferences> futurePrefs = SharedPreferences.getInstance();
    futurePrefs.then((prefs) {
      prefs.setStringList(countdown.uuid, <String>[
        countdown.name,
        countdown.finish.millisecondsSinceEpoch.toString()
      ]);
    });
  }

  int length() => _countdowns.length;

  Countdown get(int i) => _countdowns[i];

  void remove(Countdown countdown) {
    Future<SharedPreferences> futurePrefs = SharedPreferences.getInstance();
    futurePrefs.then((prefs) {
      prefs.remove(countdown.uuid);
    });
    this._countdowns.remove(countdown);
    notifyListeners();
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Countdown',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: MyHomePage(title: 'Countdowns'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void newCountdown(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (_) => CreateCountdown()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: ScopedModelDescendant<CountdownModel>(
          builder: (context, child, model) => ListView.separated(
                itemCount: model.length(),
                separatorBuilder: (context, index) => Divider(
                      color: Colors.black,
                      height: 0,
                    ),
                itemBuilder: (_, int index) => model.get(index),
              ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          newCountdown(context);
        },
        tooltip: 'New',
        child: Icon(Icons.add),
      ),
    );
  }
}
